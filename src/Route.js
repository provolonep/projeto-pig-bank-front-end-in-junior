import React from 'react'
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'
import Home from './pages/Home/Home'
import NotFound from './pages/NotFound/NotFound'
import EmConstrucao from './pages/EmConstrucao/EmConstrucao' 
import Cotacoes from './pages/Cotacao/Cotacao'

function Routes() {
    return(
        <BrowserRouter>
            <Switch>
                 <Route exact path="/">
                    <Redirect to="/Home"></Redirect>
                </Route> 
                <Route exact path="/Home">
                    <Home/>
                </Route>
                <Route exact path="/EmBreve">
                    <EmConstrucao/>
                </Route>
                <Route exact path="/Cotacoes">
                    <Cotacoes/>
                </Route>

                <Route path="/">
                    <NotFound/>
                </Route>
            </Switch>        
        </BrowserRouter>
    )
}

export default Routes