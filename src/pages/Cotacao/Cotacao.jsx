import React, {useEffect, useState} from 'react'
import './Cotacao.css'
import Header from '../../common/Header/Header'
import Footer from '../../common/Foooter/Footer'
import { PegarMoedas } from '../../services/MoedaApi'


function Cotacoes() {
    const [estado, setEstado] = useState([])
    
    useEffect(() => {
        PegarMoedas(setEstado)
    }, [])

    useEffect(() => {
        console.log(estado)
    }, [estado])

    return (<>
    <Header></Header>
    <main>
        <h1>Cotação das Moedas</h1>
        <section>
        {estado.map( bloco => {
                return (<div className = "caixas">
                    <div>
                        <p>{bloco.name}</p>
                        <h1>{bloco.code}</h1>
                    </div>
                    <div className = "direita">
                        <p>Máxima: {bloco.high}</p>
                        <p>Mínima: {bloco.low}</p>
                        <p>Atualizado em {bloco.create_date}</p>
                    </div>
                </div>)
            
        })
    }
    </section>
    </main>
    <Footer></Footer>
    </>)
}

export default Cotacoes