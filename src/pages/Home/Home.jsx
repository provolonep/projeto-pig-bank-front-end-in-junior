import React from 'react'
import './Home.css'
import Header from '../../common/Header/Header'
import Footer from '../../common/Foooter/Footer'
import Topo from '../../img/Topo.svg'
import Meio1 from '../../img/Meio1.svg'
import Meio2 from '../../img/Meio2.svg'
import Meio3 from '../../img/Meio3.svg'
import Baixo from '../../img/Baixo.svg'
import {Link} from 'react-router-dom'

function Home(){
    return<>
    <Header />
    <main>
        <section id = "secao1">
            <div>
                <h1>Ping Bank</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a leo eu enim auctor faucibus id eget dolor. Pellentesque faucibus leo sed arcu vulputate rhoncus.</p>
                <Link to ="/EmBreve">
                    <div className = "butao">NOVA META</div>
                </Link>
            </div>
            <div>
            <img src={Topo} alt="Porquinho segurando uma moeda"/>
            </div>

        </section>

        <section id = "secao2">
            <div>
                <img src={Meio1} alt="Mulher em cadeira de rodas pensando em uma grávida"/>
                <h2>DEFINA UMA META</h2>
                <p>Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis.</p>
            </div>
            <div>
                <img src={Meio2} alt="Mulher surfando nos gráficos de uma planilha"/>
                <h2>ACOMPANHE O PROGRESSO</h2>
                <p>Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis.</p>
            </div>
            <div>
                <img src={Meio3} alt="Mulher de cadeira de rodas sendo cumprimentada por outra mulher"/>
                <h2>ALCANCE SEU OBJETIVO</h2>
                <p>Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis.</p>
            </div>
        </section>

        <section id = "secao3">
            <div>
                <div id ="bloco1">
                    <h2>Já tem uma meta?</h2>
                    <p>Phasellus mollis eget enim sed malesuada. Nulla facilisi. Aliquam nunc lacus, commodo hendrerit commodo pulvinar, suscipit eget ipsum. Nulla at sem ex.</p>
                    <Link to ="/EmBreve">
                        <div className = "butao">VER METAS</div>
                    </Link>
                </div>
                <div>
                    <img src={Baixo} alt="Homem colocando moeda em um cofre de porquinho gigante"/>
                </div>
            </div>
            <div id = "bloco3">
                <h2>Não tem? Comece agora mesmo!</h2>
                <Link to ="/EmBreve">
                    <div className = "butao">NOVA META</div>
                </Link>
            </div>
        </section>
    </main>
    <Footer />
    </>
}

export default Home