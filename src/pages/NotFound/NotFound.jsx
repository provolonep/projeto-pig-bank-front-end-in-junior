import React from 'react'
import Header from '../../common/Header/Header'
import Footer from '../../common/Foooter/Footer'
import PorcoConfuso from '../../img/PorcoConfuso.svg'
import './NotFound.css'
import {Link} from 'react-router-dom'
function NotFound(){
    return <>
    <Header></Header>
    <main>
        <h1>Error 404</h1>
        <img src= {PorcoConfuso} alt="Um porquinho confuso"/>
        <p>Ops! Parece que o endereço que você digitou está incorreto...</p>
        <Link to='/Home'>
            <div className ="butao">VOLTAR</div>
        </Link>
    </main>
    <Footer></Footer>
    </>
}

export default NotFound