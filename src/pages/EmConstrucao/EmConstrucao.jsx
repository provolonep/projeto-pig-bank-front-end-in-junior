import React from 'react'
import Header from '../../common/Header/Header'
import Footer from '../../common/Foooter/Footer'
import PorcoConstrucao from '../../img/PorcoConstrucao.svg'
import './EmConstrucao.css'
import {Link} from 'react-router-dom'
function Construcao(){
    return <>
    <Header></Header>
    <main>
    <h1>Em construção</h1>
    <img src= {PorcoConstrucao} alt="Um porquinho confuso"/>
    <p>Parece que essa página ainda não foi implementada...</p>
    <p>Tente novamente mais tarde!</p>
    <Link to='/Home'>
        <div className = 'butao'>Voltar</div>
    </Link>
    </main>
    <Footer></Footer>
    </>
}

export default Construcao