import axios from 'axios'

const api = axios.create({
    baseURL: "https://economia.awesomeapi.com.br/json/"
})


async function PegarMoedas(setEstado) {

    await api.get('all')
        .then(resposta =>{
            let coinList = []
            Object.keys(resposta.data).forEach( key => {
                coinList.push(resposta.data[key])
            })
            setEstado(coinList);
        }).catch(error =>{
            console.log(error);
        })
}

export {PegarMoedas}